var _templateObject = _taggedTemplateLiteralLoose([''], ['']);

function _taggedTemplateLiteralLoose(strings, raw) { strings.raw = raw; return strings; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import '../css/App.css';
import axios from 'axios';
import { Config } from './Configs.js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import styled from 'styled-components';

// import getMuiTheme from 'material-ui/styles/getMuiTheme';
// import FlatButton from 'material-ui/FlatButton';
// import ContentAdd from 'material-ui/svg-icons/content/add';

import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import RadioButton from 'material-ui/RadioButton';
import TextField from 'material-ui/TextField';
import LinearProgress from 'material-ui/LinearProgress';
import { Collapse } from 'react-collapse';

import IconDown from './../Images/down_icon.png';
import IconUp from './../Images/up_icon.png';

import { WordingInfo } from './Localized/WordingInfo.js';
import { MSGSteateInfo, SendInfo, FeedbackInfo, options } from './Localized/MessageInfo.js';

//user:patient@ooca.co
//password:secret

var MSGSteate = { Quality: 'Quality', Feedback: 'Feedback', Problem: 'Problem', Thank: 'Thank', SendMSG: 'SendMSG', SendFail: 'SendFail' };
var _isLocal;
var _isProvider = false;
var _AppointmentID;
var _Token;
var _Property;
var _BaseURL;

var style = { marginRight: 20 };
var RaisedBtnBoxStyle = { paddingTop: '10px', margin: '0px 5px 0px 5px' };

var MessagePanel = function (_Component) {
  _inherits(MessagePanel, _Component);

  function MessagePanel(props) {
    _classCallCheck(this, MessagePanel);

    var _this = _possibleConstructorReturn(this, _Component.call(this, props));

    _this.handleOpen = function () {
      _this.setState({ open: true });
    };

    _this.handleClose = function () {
      _this.setState({ open: false, MessageState: MSGSteate.Quality, anchorOrigin: { anchorOrigin: '' } });
    };

    _this.setAnchor = function (positionElement, position) {
      var anchorOrigin = _this.state.anchorOrigin;

      anchorOrigin[positionElement] = position;

      FeedbackInfo.problem = anchorOrigin.vertical;
      _this.setState({ anchorOrigin: anchorOrigin });
    };

    var _this$props = _this.props,
        AppointmentID = _this$props.AppointmentID,
        Local = _this$props.Local,
        Token = _this$props.Token,
        isProvider = _this$props.isProvider,
        Property = _this$props.Property,
        baseURL = _this$props.baseURL;

    console.warn('### : ', _this.props);

    _this.state = { open: !!AppointmentID ? true : false, MessageState: MSGSteate.Quality, anchorOrigin: { anchorOrigin: '' } };

    _isLocal = !!Local ? Local.toUpperCase().match(/EN/i) ? 'EN' : 'TH' : 'EN';
    _isProvider = !!isProvider ? isProvider : false;
    _AppointmentID = AppointmentID;
    _Property = function _Property() {
      return Property;
    };
    _Token = Token;
    _BaseURL = baseURL;
    return _this;
  }

  MessagePanel.prototype.render = function render() {
    return React.createElement(
      'div',
      { className: 'message-panel' },
      this.DisplayMessage(this.state.MessageState)
    );
  };

  MessagePanel.prototype.DisplayMessage = function DisplayMessage(_state) {
    var _isActive = this.state.MessageState.match(/(Quality|Feedback|SendMSG|SendFail)/i) ? true : false;
    //1 (_state===MSGSteate.Feedback)||(_state===MSGSteate.SendMSG)||(_state===MSGSteate.Quality)
    //2 (_state===MSGSteate.Problem)||(_state===MSGSteate.Thank)

    return React.createElement(
      MuiThemeProvider,
      null,
      React.createElement(
        Dialog,
        { actions: undefined, modal: true, open: this.state.open, bodyStyle: { padding: '20px 10px 20px 10px' },
          onRequestClose: this.handleClose, contentStyle: { width: '90%' }, style: {} },
        React.createElement(
          Collapse,
          { isOpened: _isActive },
          this.ActiveOnState(_state)
        ),
        React.createElement(
          Collapse,
          { isOpened: !_isActive },
          this.ActiveOnState(_state)
        )
      )
    );
  };

  MessagePanel.prototype.handCallBack = function handCallBack() {
    if (!!_Property) _Property();

    this.handleClose();
  };

  MessagePanel.prototype.handMSGState = function handMSGState(_state) {
    this.setState({ MessageState: _state });
  };

  MessagePanel.prototype.handFeedbackInfo = function handFeedbackInfo() {
    FeedbackInfo.rating = true;
    FeedbackInfo.problem = null;
    FeedbackInfo.problem_other = null;
    FeedbackInfo.feedback = null;
  };

  MessagePanel.prototype.ActiveOnState = function ActiveOnState(_state) {
    var _this2 = this;

    var OuttaDiv = styled.div.attrs({ className: 'App-MessageBox' })(_templateObject);

    switch (_state) {
      case MSGSteate.Quality:
        {
          return React.createElement(
            OuttaDiv,
            { style: { overflowX: 'hidden' } },
            React.createElement(
              'div',
              { style: { paddingTop: '20px', marginBottom: '30px' } },
              React.createElement(
                'label',
                { style: { margin: '10px', width: '100%' } },
                MSGSteateInfo.Quality[_isLocal]
              )
            ),
            React.createElement(
              'div',
              { style: { marginBottom: '25px', height: '100%', paddingLeft: '20px', marginLeft: '15px' } },
              React.createElement(
                FloatingActionButton,
                { mini: true, style: style,
                  onClick: function onClick() {
                    FeedbackInfo.rating = true;_this2.handMSGState(MSGSteate.Feedback);
                  } },
                React.createElement('img', { src: IconUp, className: 'App-icon', style: { margin: '-15px', height: '70px' }, alt: 'Like' })
              ),
              React.createElement(
                FloatingActionButton,
                { mini: true, style: style,
                  onClick: function onClick() {
                    FeedbackInfo.rating = false;_this2.handMSGState(MSGSteate.Problem);
                  } },
                React.createElement('img', { src: IconDown, className: 'App-icon', style: { margin: '-15px', height: '70px' }, alt: 'UnLink' })
              )
            )
          );
        }

      case MSGSteate.Feedback:
        {
          return React.createElement(
            OuttaDiv,
            { style: { overflowX: 'hidden' } },
            React.createElement(
              'div',
              null,
              React.createElement(
                'label',
                { style: { margin: '10px', width: '100%' } },
                MSGSteateInfo.Feedback[_isLocal]
              )
            ),
            React.createElement(
              'div',
              { style: { textAlign: 'left' } },
              React.createElement(TextField, { hintText: WordingInfo.Recommend[_isLocal], floatingLabelText: WordingInfo.Feedback[_isLocal],
                multiLine: true, fullWidth: true, rows: 2, onChange: function onChange(e, _value) {
                  FeedbackInfo.feedback = _value;
                } })
            ),
            React.createElement(
              'div',
              { style: RaisedBtnBoxStyle },
              React.createElement(RaisedButton, { label: WordingInfo.Skip[_isLocal], fullWidth: true, style: style, onClick: function onClick() {
                  _this2.SendFeedback();_this2.handMSGState(MSGSteate.SendMSG);
                } })
            ),
            React.createElement(
              'div',
              { style: RaisedBtnBoxStyle },
              React.createElement(RaisedButton, { label: WordingInfo.Submit[_isLocal], primary: true, fullWidth: true, style: style, onClick: function onClick() {
                  _this2.SendFeedback();_this2.handMSGState(MSGSteate.SendMSG);
                } })
            )
          );
        }

      case MSGSteate.Problem:
        {
          return React.createElement(
            OuttaDiv,
            { style: { overflowX: 'hidden' } },
            React.createElement(
              'div',
              null,
              React.createElement(
                'label',
                { style: { margin: '10px', width: '100%' } },
                MSGSteateInfo.Problem[_isLocal]
              )
            ),
            React.createElement(
              'div',
              { style: { paddingTop: '15px', marginBottom: '25px' } },
              React.createElement(
                'div',
                { style: { width: '50%', minWidth: '250px', textAlign: '-webkit-left' } },
                React.createElement(RadioButton, { onClick: this.setAnchor.bind(this, 'vertical', options.not_completed),
                  label: WordingInfo.VideoProblem[_isLocal], checked: this.state.anchorOrigin.vertical === options.not_completed }),
                React.createElement(RadioButton, { onClick: this.setAnchor.bind(this, 'vertical', options.unstable),
                  label: WordingInfo.VideoConnection[_isLocal], checked: this.state.anchorOrigin.vertical === options.unstable }),
                React.createElement(RadioButton, { onClick: this.setAnchor.bind(this, 'vertical', options.other),
                  label: WordingInfo.Others[_isLocal], checked: this.state.anchorOrigin.vertical === options.other })
              ),
              this.state.anchorOrigin.vertical === options.other ? React.createElement(
                'div',
                { style: { textAlign: 'left', marginBottom: '0px' } },
                React.createElement(TextField, { hintText: WordingInfo.Recommend[_isLocal], floatingLabelText: WordingInfo.Feedback[_isLocal],
                  multiLine: true, fullWidth: true, rows: 2, onChange: function onChange(e, _value) {
                    FeedbackInfo.problem_other = _value;
                  } })
              ) : undefined
            ),
            React.createElement(
              'div',
              { style: RaisedBtnBoxStyle },
              React.createElement(RaisedButton, { label: WordingInfo.Skip[_isLocal], fullWidth: true, style: style, onClick: function onClick() {
                  _this2.SendFeedback();_this2.handMSGState(MSGSteate.SendMSG);
                } })
            ),
            React.createElement(
              'div',
              { style: RaisedBtnBoxStyle },
              React.createElement(RaisedButton, { label: WordingInfo.Submit[_isLocal], primary: true, fullWidth: true, style: style, onClick: function onClick() {
                  _this2.SendFeedback();_this2.handMSGState(MSGSteate.SendMSG);
                } })
            )
          );
        }

      case MSGSteate.SendMSG:
        {
          return React.createElement(
            OuttaDiv,
            { style: { overflowX: 'hidden' } },
            React.createElement(
              'div',
              { style: { paddingTop: '20px', marginBottom: '30px' } },
              React.createElement(
                'label',
                { style: { margin: '10px', width: '100%' } },
                MSGSteateInfo.SendMSG[_isLocal]
              )
            ),
            React.createElement(
              'div',
              { style: { padding: '0px 10px 0px 10px', marginBottom: '30px' } },
              React.createElement(LinearProgress, { mode: 'indeterminate' })
            ),
            React.createElement(
              'div',
              { style: { width: '100%', paddingLeft: '10px' } },
              React.createElement(RaisedButton, { label: WordingInfo.Continue[_isLocal], primary: true, style: style, onClick: function onClick() {
                  return _this2.handMSGState(MSGSteate.Thank);
                } })
            )
          );
        }

      case MSGSteate.Thank:
        {
          return React.createElement(
            OuttaDiv,
            { style: { overflowX: 'hidden' } },
            React.createElement(
              'div',
              { style: { paddingTop: '20px', marginBottom: '30px' } },
              React.createElement(
                'label',
                { style: { margin: '10px', width: '100%' } },
                MSGSteateInfo.Thank[_isLocal]
              )
            ),
            React.createElement(
              'div',
              { style: { width: '100%', paddingLeft: '10px' } },
              React.createElement(RaisedButton, { label: WordingInfo.Continue[_isLocal], primary: true, style: style, onClick: this.handleClose }),
              _isProvider ? React.createElement(RaisedButton, { label: WordingInfo.CallBack[_isLocal], primary: true, style: style, onClick: function onClick() {
                  _this2.handCallBack();
                } }) : undefined
            )
          );
        }
      default:
        {}
    }
  };

  MessagePanel.prototype.SendFeedback = function SendFeedback() {
    var _this3 = this;

    var Feedback_API = !!_BaseURL ? _BaseURL + 'appointments/' + _AppointmentID + '/provider-feedback' : Config().api_feedback + '/' + (!!_AppointmentID ? _AppointmentID : '1') + '/provider-feedback';

    console.log("### FeedbackInfo :", FeedbackInfo, ' api : ', Feedback_API);
    var header = { 'Content-Type': 'application/json', 'Authorization': _Token }; //Authorization':`Bearer${'jwt token'}`
    var request = axios({ url: Feedback_API, method: 'POST', headers: header, data: FeedbackInfo, dataType: 'json' });

    request.then(function (response) {
      console.log("[API] FeedbackInfo : ", response);
      if (_this3.state.MessageState === MSGSteate.SendMSG) _this3.handMSGState(MSGSteate.Thank);

      _this3.handFeedbackInfo();
    }).catch(function (error) {
      console.log("FeedbackInfo fail", error);
      if (_this3.state.MessageState === MSGSteate.SendMSG) _this3.handMSGState(MSGSteate.Thank);
      //this.handMSGState(MSGSteate.Fail);
      _this3.handFeedbackInfo();
    });
  };

  return MessagePanel;
}(Component);

export default MessagePanel;
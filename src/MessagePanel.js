import React, { Component } from 'react';
import '../css/App.css';
import axios from 'axios';
import {Config} from './Configs.js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import styled from 'styled-components';

// import getMuiTheme from 'material-ui/styles/getMuiTheme';
// import FlatButton from 'material-ui/FlatButton';
// import ContentAdd from 'material-ui/svg-icons/content/add';

import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import RadioButton from 'material-ui/RadioButton';
import TextField from 'material-ui/TextField';
import LinearProgress from 'material-ui/LinearProgress';
import {Collapse} from 'react-collapse';

import IconDown from './../Images/down_icon.png';
import IconUp from './../Images/up_icon.png';

import {WordingInfo} from './Localized/WordingInfo.js'
import {MSGSteateInfo,SendInfo,FeedbackInfo,options} from './Localized/MessageInfo.js'

//user:patient@ooca.co
//password:secret

const MSGSteate={Quality:'Quality',Feedback:'Feedback',Problem:'Problem',Thank:'Thank',SendMSG:'SendMSG',SendFail:'SendFail'};
var _isLocal;
var _isProvider=false;
var _AppointmentID;
var _Token;
var _Property;
var _BaseURL;

const style = {marginRight:20,};
const RaisedBtnBoxStyle={paddingTop:'10px',margin:'0px 5px 0px 5px'};

class MessagePanel extends Component 
{
    constructor(props)
    {
      super(props);
      const {AppointmentID,Local,Token,isProvider,Property,baseURL}=this.props;
      console.warn('### : ',this.props);

      this.state= {open:(!!AppointmentID)?true:false,MessageState:MSGSteate.Quality,anchorOrigin:{anchorOrigin:''}};

      _isLocal=(!!Local)?(Local.toUpperCase().match(/EN/i)?'EN':'TH'):'EN';
      _isProvider=(!!isProvider)?isProvider:false;
      _AppointmentID=AppointmentID;
      _Property=()=>Property;
      _Token=Token;
      _BaseURL=baseURL;
    }

    render() 
    {
      return (<div className='message-panel'>{this.DisplayMessage(this.state.MessageState)}</div>);
    }

    DisplayMessage(_state)
    {
      const _isActive=(this.state.MessageState.match(/(Quality|Feedback|SendMSG|SendFail)/i)?true:false);
      //1 (_state===MSGSteate.Feedback)||(_state===MSGSteate.SendMSG)||(_state===MSGSteate.Quality)
      //2 (_state===MSGSteate.Problem)||(_state===MSGSteate.Thank)

      return <MuiThemeProvider>            
              <Dialog actions={undefined} modal={true} open={this.state.open} bodyStyle={{padding:'20px 10px 20px 10px'}}
                      onRequestClose={this.handleClose} contentStyle={{width:'90%'}} style={{}}>

              <Collapse isOpened={_isActive}>{this.ActiveOnState(_state)}</Collapse>
              <Collapse isOpened={!_isActive}>{this.ActiveOnState(_state)}</Collapse>     
              </Dialog>            
            </MuiThemeProvider>
    }
  
    handleOpen = () => {this.setState({open:true});};
    handleClose = () => {this.setState({open:false,MessageState:MSGSteate.Quality,anchorOrigin:{anchorOrigin:''}});};

    handCallBack()
    {
      if(!!_Property)
         _Property();

      this.handleClose();
    }

    handMSGState(_state)
    {
      this.setState({MessageState:_state})
    }

    handFeedbackInfo()
    {
      FeedbackInfo.rating=true;
      FeedbackInfo.problem=null;
      FeedbackInfo.problem_other=null;
      FeedbackInfo.feedback=null;
    }

    setAnchor = (positionElement, position) => 
    {
      const {anchorOrigin} = this.state;
      anchorOrigin[positionElement] = position;

      FeedbackInfo.problem=anchorOrigin.vertical;
      this.setState({anchorOrigin:anchorOrigin,});
    };

    ActiveOnState(_state)
    {
      const OuttaDiv = styled.div.attrs({className:'App-MessageBox'})``;

      switch(_state)
      {
        case MSGSteate.Quality : 
             { 
               return(<OuttaDiv style={{overflowX:'hidden'}}>
                        <div style={{paddingTop:'20px',marginBottom:'30px'}}>
                          <label style={{margin:'10px',width:'100%'}}>{MSGSteateInfo.Quality[_isLocal]}</label>
                        </div>
                        <div style={{marginBottom:'25px',height:'100%',paddingLeft:'20px',marginLeft:'15px'}}>
                          <FloatingActionButton mini={true} style={style} 
                            onClick={()=>{FeedbackInfo.rating=true; this.handMSGState(MSGSteate.Feedback)}}>
                            <img src={IconUp} className='App-icon' style={{margin:'-15px',height:'70px'}} alt='Like' />
                          </FloatingActionButton>
                          <FloatingActionButton mini={true} style={style} 
                            onClick={()=>{FeedbackInfo.rating=false; this.handMSGState(MSGSteate.Problem)}}>
                            <img src={IconDown} className='App-icon' style={{margin:'-15px',height:'70px'}} alt='UnLink' />
                          </FloatingActionButton>
                        </div>
                      </OuttaDiv>);
            } 

         case MSGSteate.Feedback : 
            { 
              return(<OuttaDiv style={{overflowX:'hidden'}}>
                        <div ><label style={{margin:'10px',width:'100%'}}>{MSGSteateInfo.Feedback[_isLocal]}</label></div>
                        <div style={{textAlign:'left'}}>
                        <TextField hintText={WordingInfo.Recommend[_isLocal]} floatingLabelText={WordingInfo.Feedback[_isLocal]}
                                  multiLine={true} fullWidth={true} rows={2} onChange={(e,_value)=>{FeedbackInfo.feedback=_value;}}/>
                        </div>
                        <div style={RaisedBtnBoxStyle}>
                          <RaisedButton label={WordingInfo.Skip[_isLocal]} fullWidth={true} style={style} onClick={()=>{this.SendFeedback(); this.handMSGState(MSGSteate.SendMSG)}}/>
                        </div>
                        <div style={RaisedBtnBoxStyle}>
                          <RaisedButton label={WordingInfo.Submit[_isLocal]} primary={true} fullWidth={true} style={style} onClick={()=>{this.SendFeedback(); this.handMSGState(MSGSteate.SendMSG)}}/>
                        </div>
                      </OuttaDiv>);
           } 

         case MSGSteate.Problem : 
             { 
                return (<OuttaDiv style={{overflowX:'hidden'}}>
                          <div><label style={{margin:'10px',width:'100%'}}>{MSGSteateInfo.Problem[_isLocal]}</label></div>
                          <div style={{paddingTop:'15px',marginBottom:'25px'}}>
                            <div style={{width:'50%',minWidth:'250px',textAlign:'-webkit-left'}}>
                              <RadioButton onClick={this.setAnchor.bind(this,'vertical',options.not_completed)}
                                label={WordingInfo.VideoProblem[_isLocal]} checked={this.state.anchorOrigin.vertical===options.not_completed}/>

                              <RadioButton onClick={this.setAnchor.bind(this,'vertical',options.unstable)}
                                label={WordingInfo.VideoConnection[_isLocal]} checked={this.state.anchorOrigin.vertical===options.unstable}/>

                              <RadioButton onClick={this.setAnchor.bind(this,'vertical',options.other)}
                                label={WordingInfo.Others[_isLocal]} checked={this.state.anchorOrigin.vertical===options.other}/>
                          </div>
                          {(this.state.anchorOrigin.vertical===options.other)?
                            <div style={{textAlign:'left',marginBottom:'0px'}}>
                              <TextField hintText={WordingInfo.Recommend[_isLocal]} floatingLabelText={WordingInfo.Feedback[_isLocal]}
                                        multiLine={true} fullWidth={true} rows={2} onChange={(e,_value)=>{FeedbackInfo.problem_other=_value;}}/>
                            </div>:undefined}
                          </div>
                          <div style={RaisedBtnBoxStyle}>
                            <RaisedButton label={WordingInfo.Skip[_isLocal]} fullWidth={true} style={style} onClick={()=>{this.SendFeedback(); this.handMSGState(MSGSteate.SendMSG)}}/>
                          </div>
                          <div style={RaisedBtnBoxStyle}>
                            <RaisedButton label={WordingInfo.Submit[_isLocal]} primary={true} fullWidth={true} style={style} onClick={()=>{this.SendFeedback(); this.handMSGState(MSGSteate.SendMSG)}}/>
                          </div>
                        </OuttaDiv>);
             } 
        
        case MSGSteate.SendMSG :
        {
              return (<OuttaDiv style={{overflowX:'hidden'}}>
                        <div style={{paddingTop:'20px',marginBottom:'30px'}}>
                          <label style={{margin:'10px',width:'100%'}}>{MSGSteateInfo.SendMSG[_isLocal]}</label>
                        </div>
                        <div style={{padding:'0px 10px 0px 10px',marginBottom:'30px'}}>
                            <LinearProgress mode='indeterminate' />
                        </div>
                        <div style={{width:'100%',paddingLeft:'10px'}}>
                            <RaisedButton label={WordingInfo.Continue[_isLocal]} primary={true} style={style} onClick={()=>this.handMSGState(MSGSteate.Thank)}/>
                        </div>
                      </OuttaDiv>);
        }

        case MSGSteate.Thank : 
             { 
               return (<OuttaDiv style={{overflowX:'hidden'}}>
                          <div style={{paddingTop:'20px',marginBottom:'30px'}}>
                            <label style={{margin:'10px',width:'100%'}}>{MSGSteateInfo.Thank[_isLocal]}</label>
                          </div>
                          <div style={{width:'100%',paddingLeft:'10px'}}>
                            <RaisedButton label={WordingInfo.Continue[_isLocal]} primary={true} style={style} onClick={this.handleClose}/>
                            {(_isProvider)?<RaisedButton label={WordingInfo.CallBack[_isLocal]} primary={true} style={style} onClick={()=>{this.handCallBack();}}/>:undefined}
                          </div>
                       </OuttaDiv>);
             } 
        default : { }
      }
    }

    SendFeedback() 
    {
        const Feedback_API=(!!_BaseURL)?`${_BaseURL}appointments/${_AppointmentID}/provider-feedback`:`${Config().api_feedback}/${(!!_AppointmentID)?_AppointmentID:'1'}/provider-feedback`;

        console.log("### FeedbackInfo :",FeedbackInfo,' api : ',Feedback_API); 
        const header = {'Content-Type':'application/json','Authorization':_Token}; //Authorization':`Bearer${'jwt token'}`
        const request = axios({url:Feedback_API,method:'POST',headers:header,data:FeedbackInfo,dataType:'json',});

        request.then((response)=> 
        {
          console.log("[API] FeedbackInfo : ", response);
          if(this.state.MessageState===MSGSteate.SendMSG)
             this.handMSGState(MSGSteate.Thank);

             this.handFeedbackInfo();
        }).catch((error)=> 
        {
          console.log("FeedbackInfo fail", error);
          if(this.state.MessageState===MSGSteate.SendMSG)
             this.handMSGState(MSGSteate.Thank);
             //this.handMSGState(MSGSteate.Fail);
             this.handFeedbackInfo();
        })
    }

}

export default MessagePanel;

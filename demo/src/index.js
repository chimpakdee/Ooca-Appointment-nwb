import React, {Component} from 'react'
import {render} from 'react-dom'
import '../../css/App.css';

import Example from '../../src'

class Demo extends Component 
{
  render() 
  {
    return <div className='App'>
            <h1>appointment-pack-o Demo</h1>
            <Example Local={'TH'} AppointmentID={'96zwxk4rqkvlg5e238vm'}/>
           </div>
  }
}

render(<Demo/>, document.querySelector('#demo'))

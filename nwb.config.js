module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: {
      global: 'o-appointment-nwb',
      externals: {
        react: 'React'
      }
    }
  }
}

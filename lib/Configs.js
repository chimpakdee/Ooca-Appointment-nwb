"use strict";

var dev_api = "https://private-anon-65a255ffbb-ooca.apiary-mock.com";

var production_api = "http://api.ooca.co";

var header = { 'Content-Type': 'application/json' };
//'Authorization':Bearer{jwt token}
//,'content-type':'application/x-www-form-urlencoded'
//'x-master-key': `smelink1234`,'Cache-Control':['no-cache','no-store','must-revalidate',]

var dev = { api_feedback: dev_api + "/api/appointments"
};

var production = { api_feedback: production_api + "/api/appointments"
};

exports.Config = function () {
                return dev;
};
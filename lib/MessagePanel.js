'use strict';

exports.__esModule = true;

var _templateObject = _taggedTemplateLiteralLoose([''], ['']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

require('../css/App.css');

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _Configs = require('./Configs.js');

var _MuiThemeProvider = require('material-ui/styles/MuiThemeProvider');

var _MuiThemeProvider2 = _interopRequireDefault(_MuiThemeProvider);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _Dialog = require('material-ui/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _FloatingActionButton = require('material-ui/FloatingActionButton');

var _FloatingActionButton2 = _interopRequireDefault(_FloatingActionButton);

var _RadioButton = require('material-ui/RadioButton');

var _RadioButton2 = _interopRequireDefault(_RadioButton);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _LinearProgress = require('material-ui/LinearProgress');

var _LinearProgress2 = _interopRequireDefault(_LinearProgress);

var _reactCollapse = require('react-collapse');

var _down_icon = require('./../Images/down_icon.png');

var _down_icon2 = _interopRequireDefault(_down_icon);

var _up_icon = require('./../Images/up_icon.png');

var _up_icon2 = _interopRequireDefault(_up_icon);

var _WordingInfo = require('./Localized/WordingInfo.js');

var _MessageInfo = require('./Localized/MessageInfo.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteralLoose(strings, raw) { strings.raw = raw; return strings; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// import getMuiTheme from 'material-ui/styles/getMuiTheme';
// import FlatButton from 'material-ui/FlatButton';
// import ContentAdd from 'material-ui/svg-icons/content/add';

//user:patient@ooca.co
//password:secret

var MSGSteate = { Quality: 'Quality', Feedback: 'Feedback', Problem: 'Problem', Thank: 'Thank', SendMSG: 'SendMSG', SendFail: 'SendFail' };
var _isLocal;
var _isProvider = false;
var _AppointmentID;
var _Token;
var _Property;
var _BaseURL;

var style = { marginRight: 20 };
var RaisedBtnBoxStyle = { paddingTop: '10px', margin: '0px 5px 0px 5px' };

var MessagePanel = function (_Component) {
  _inherits(MessagePanel, _Component);

  function MessagePanel(props) {
    _classCallCheck(this, MessagePanel);

    var _this = _possibleConstructorReturn(this, _Component.call(this, props));

    _this.handleOpen = function () {
      _this.setState({ open: true });
    };

    _this.handleClose = function () {
      _this.setState({ open: false, MessageState: MSGSteate.Quality, anchorOrigin: { anchorOrigin: '' } });
    };

    _this.setAnchor = function (positionElement, position) {
      var anchorOrigin = _this.state.anchorOrigin;

      anchorOrigin[positionElement] = position;

      _MessageInfo.FeedbackInfo.problem = anchorOrigin.vertical;
      _this.setState({ anchorOrigin: anchorOrigin });
    };

    var _this$props = _this.props,
        AppointmentID = _this$props.AppointmentID,
        Local = _this$props.Local,
        Token = _this$props.Token,
        isProvider = _this$props.isProvider,
        Property = _this$props.Property,
        baseURL = _this$props.baseURL;

    console.warn('### : ', _this.props);

    _this.state = { open: !!AppointmentID ? true : false, MessageState: MSGSteate.Quality, anchorOrigin: { anchorOrigin: '' } };

    _isLocal = !!Local ? Local.toUpperCase().match(/EN/i) ? 'EN' : 'TH' : 'EN';
    _isProvider = !!isProvider ? isProvider : false;
    _AppointmentID = AppointmentID;
    _Property = function _Property() {
      return Property;
    };
    _Token = Token;
    _BaseURL = baseURL;
    return _this;
  }

  MessagePanel.prototype.render = function render() {
    return _react2.default.createElement(
      'div',
      { className: 'message-panel' },
      this.DisplayMessage(this.state.MessageState)
    );
  };

  MessagePanel.prototype.DisplayMessage = function DisplayMessage(_state) {
    var _isActive = this.state.MessageState.match(/(Quality|Feedback|SendMSG|SendFail)/i) ? true : false;
    //1 (_state===MSGSteate.Feedback)||(_state===MSGSteate.SendMSG)||(_state===MSGSteate.Quality)
    //2 (_state===MSGSteate.Problem)||(_state===MSGSteate.Thank)

    return _react2.default.createElement(
      _MuiThemeProvider2.default,
      null,
      _react2.default.createElement(
        _Dialog2.default,
        { actions: undefined, modal: true, open: this.state.open, bodyStyle: { padding: '20px 10px 20px 10px' },
          onRequestClose: this.handleClose, contentStyle: { width: '90%' }, style: {} },
        _react2.default.createElement(
          _reactCollapse.Collapse,
          { isOpened: _isActive },
          this.ActiveOnState(_state)
        ),
        _react2.default.createElement(
          _reactCollapse.Collapse,
          { isOpened: !_isActive },
          this.ActiveOnState(_state)
        )
      )
    );
  };

  MessagePanel.prototype.handCallBack = function handCallBack() {
    if (!!_Property) _Property();

    this.handleClose();
  };

  MessagePanel.prototype.handMSGState = function handMSGState(_state) {
    this.setState({ MessageState: _state });
  };

  MessagePanel.prototype.handFeedbackInfo = function handFeedbackInfo() {
    _MessageInfo.FeedbackInfo.rating = true;
    _MessageInfo.FeedbackInfo.problem = null;
    _MessageInfo.FeedbackInfo.problem_other = null;
    _MessageInfo.FeedbackInfo.feedback = null;
  };

  MessagePanel.prototype.ActiveOnState = function ActiveOnState(_state) {
    var _this2 = this;

    var OuttaDiv = _styledComponents2.default.div.attrs({ className: 'App-MessageBox' })(_templateObject);

    switch (_state) {
      case MSGSteate.Quality:
        {
          return _react2.default.createElement(
            OuttaDiv,
            { style: { overflowX: 'hidden' } },
            _react2.default.createElement(
              'div',
              { style: { paddingTop: '20px', marginBottom: '30px' } },
              _react2.default.createElement(
                'label',
                { style: { margin: '10px', width: '100%' } },
                _MessageInfo.MSGSteateInfo.Quality[_isLocal]
              )
            ),
            _react2.default.createElement(
              'div',
              { style: { marginBottom: '25px', height: '100%', paddingLeft: '20px', marginLeft: '15px' } },
              _react2.default.createElement(
                _FloatingActionButton2.default,
                { mini: true, style: style,
                  onClick: function onClick() {
                    _MessageInfo.FeedbackInfo.rating = true;_this2.handMSGState(MSGSteate.Feedback);
                  } },
                _react2.default.createElement('img', { src: _up_icon2.default, className: 'App-icon', style: { margin: '-15px', height: '70px' }, alt: 'Like' })
              ),
              _react2.default.createElement(
                _FloatingActionButton2.default,
                { mini: true, style: style,
                  onClick: function onClick() {
                    _MessageInfo.FeedbackInfo.rating = false;_this2.handMSGState(MSGSteate.Problem);
                  } },
                _react2.default.createElement('img', { src: _down_icon2.default, className: 'App-icon', style: { margin: '-15px', height: '70px' }, alt: 'UnLink' })
              )
            )
          );
        }

      case MSGSteate.Feedback:
        {
          return _react2.default.createElement(
            OuttaDiv,
            { style: { overflowX: 'hidden' } },
            _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                'label',
                { style: { margin: '10px', width: '100%' } },
                _MessageInfo.MSGSteateInfo.Feedback[_isLocal]
              )
            ),
            _react2.default.createElement(
              'div',
              { style: { textAlign: 'left' } },
              _react2.default.createElement(_TextField2.default, { hintText: _WordingInfo.WordingInfo.Recommend[_isLocal], floatingLabelText: _WordingInfo.WordingInfo.Feedback[_isLocal],
                multiLine: true, fullWidth: true, rows: 2, onChange: function onChange(e, _value) {
                  _MessageInfo.FeedbackInfo.feedback = _value;
                } })
            ),
            _react2.default.createElement(
              'div',
              { style: RaisedBtnBoxStyle },
              _react2.default.createElement(_RaisedButton2.default, { label: _WordingInfo.WordingInfo.Skip[_isLocal], fullWidth: true, style: style, onClick: function onClick() {
                  _this2.SendFeedback();_this2.handMSGState(MSGSteate.SendMSG);
                } })
            ),
            _react2.default.createElement(
              'div',
              { style: RaisedBtnBoxStyle },
              _react2.default.createElement(_RaisedButton2.default, { label: _WordingInfo.WordingInfo.Submit[_isLocal], primary: true, fullWidth: true, style: style, onClick: function onClick() {
                  _this2.SendFeedback();_this2.handMSGState(MSGSteate.SendMSG);
                } })
            )
          );
        }

      case MSGSteate.Problem:
        {
          return _react2.default.createElement(
            OuttaDiv,
            { style: { overflowX: 'hidden' } },
            _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                'label',
                { style: { margin: '10px', width: '100%' } },
                _MessageInfo.MSGSteateInfo.Problem[_isLocal]
              )
            ),
            _react2.default.createElement(
              'div',
              { style: { paddingTop: '15px', marginBottom: '25px' } },
              _react2.default.createElement(
                'div',
                { style: { width: '50%', minWidth: '250px', textAlign: '-webkit-left' } },
                _react2.default.createElement(_RadioButton2.default, { onClick: this.setAnchor.bind(this, 'vertical', _MessageInfo.options.not_completed),
                  label: _WordingInfo.WordingInfo.VideoProblem[_isLocal], checked: this.state.anchorOrigin.vertical === _MessageInfo.options.not_completed }),
                _react2.default.createElement(_RadioButton2.default, { onClick: this.setAnchor.bind(this, 'vertical', _MessageInfo.options.unstable),
                  label: _WordingInfo.WordingInfo.VideoConnection[_isLocal], checked: this.state.anchorOrigin.vertical === _MessageInfo.options.unstable }),
                _react2.default.createElement(_RadioButton2.default, { onClick: this.setAnchor.bind(this, 'vertical', _MessageInfo.options.other),
                  label: _WordingInfo.WordingInfo.Others[_isLocal], checked: this.state.anchorOrigin.vertical === _MessageInfo.options.other })
              ),
              this.state.anchorOrigin.vertical === _MessageInfo.options.other ? _react2.default.createElement(
                'div',
                { style: { textAlign: 'left', marginBottom: '0px' } },
                _react2.default.createElement(_TextField2.default, { hintText: _WordingInfo.WordingInfo.Recommend[_isLocal], floatingLabelText: _WordingInfo.WordingInfo.Feedback[_isLocal],
                  multiLine: true, fullWidth: true, rows: 2, onChange: function onChange(e, _value) {
                    _MessageInfo.FeedbackInfo.problem_other = _value;
                  } })
              ) : undefined
            ),
            _react2.default.createElement(
              'div',
              { style: RaisedBtnBoxStyle },
              _react2.default.createElement(_RaisedButton2.default, { label: _WordingInfo.WordingInfo.Skip[_isLocal], fullWidth: true, style: style, onClick: function onClick() {
                  _this2.SendFeedback();_this2.handMSGState(MSGSteate.SendMSG);
                } })
            ),
            _react2.default.createElement(
              'div',
              { style: RaisedBtnBoxStyle },
              _react2.default.createElement(_RaisedButton2.default, { label: _WordingInfo.WordingInfo.Submit[_isLocal], primary: true, fullWidth: true, style: style, onClick: function onClick() {
                  _this2.SendFeedback();_this2.handMSGState(MSGSteate.SendMSG);
                } })
            )
          );
        }

      case MSGSteate.SendMSG:
        {
          return _react2.default.createElement(
            OuttaDiv,
            { style: { overflowX: 'hidden' } },
            _react2.default.createElement(
              'div',
              { style: { paddingTop: '20px', marginBottom: '30px' } },
              _react2.default.createElement(
                'label',
                { style: { margin: '10px', width: '100%' } },
                _MessageInfo.MSGSteateInfo.SendMSG[_isLocal]
              )
            ),
            _react2.default.createElement(
              'div',
              { style: { padding: '0px 10px 0px 10px', marginBottom: '30px' } },
              _react2.default.createElement(_LinearProgress2.default, { mode: 'indeterminate' })
            ),
            _react2.default.createElement(
              'div',
              { style: { width: '100%', paddingLeft: '10px' } },
              _react2.default.createElement(_RaisedButton2.default, { label: _WordingInfo.WordingInfo.Continue[_isLocal], primary: true, style: style, onClick: function onClick() {
                  return _this2.handMSGState(MSGSteate.Thank);
                } })
            )
          );
        }

      case MSGSteate.Thank:
        {
          return _react2.default.createElement(
            OuttaDiv,
            { style: { overflowX: 'hidden' } },
            _react2.default.createElement(
              'div',
              { style: { paddingTop: '20px', marginBottom: '30px' } },
              _react2.default.createElement(
                'label',
                { style: { margin: '10px', width: '100%' } },
                _MessageInfo.MSGSteateInfo.Thank[_isLocal]
              )
            ),
            _react2.default.createElement(
              'div',
              { style: { width: '100%', paddingLeft: '10px' } },
              _react2.default.createElement(_RaisedButton2.default, { label: _WordingInfo.WordingInfo.Continue[_isLocal], primary: true, style: style, onClick: this.handleClose }),
              _isProvider ? _react2.default.createElement(_RaisedButton2.default, { label: _WordingInfo.WordingInfo.CallBack[_isLocal], primary: true, style: style, onClick: function onClick() {
                  _this2.handCallBack();
                } }) : undefined
            )
          );
        }
      default:
        {}
    }
  };

  MessagePanel.prototype.SendFeedback = function SendFeedback() {
    var _this3 = this;

    var Feedback_API = !!_BaseURL ? _BaseURL + 'appointments/' + _AppointmentID + '/provider-feedback' : (0, _Configs.Config)().api_feedback + '/' + (!!_AppointmentID ? _AppointmentID : '1') + '/provider-feedback';

    console.log("### FeedbackInfo :", _MessageInfo.FeedbackInfo, ' api : ', Feedback_API);
    var header = { 'Content-Type': 'application/json', 'Authorization': _Token }; //Authorization':`Bearer${'jwt token'}`
    var request = (0, _axios2.default)({ url: Feedback_API, method: 'POST', headers: header, data: _MessageInfo.FeedbackInfo, dataType: 'json' });

    request.then(function (response) {
      console.log("[API] FeedbackInfo : ", response);
      if (_this3.state.MessageState === MSGSteate.SendMSG) _this3.handMSGState(MSGSteate.Thank);

      _this3.handFeedbackInfo();
    }).catch(function (error) {
      console.log("FeedbackInfo fail", error);
      if (_this3.state.MessageState === MSGSteate.SendMSG) _this3.handMSGState(MSGSteate.Thank);
      //this.handMSGState(MSGSteate.Fail);
      _this3.handFeedbackInfo();
    });
  };

  return MessagePanel;
}(_react.Component);

exports.default = MessagePanel;
module.exports = exports['default'];